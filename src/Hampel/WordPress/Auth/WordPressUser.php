<?php namespace Hampel\WordPress\Auth;

use Eloquent;
use Config;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class WordPressUser extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('user_pass');

	public function __construct(array $attributes = array())
	{
		parent::__construct($attributes);

		$this->primaryKey = Config::get('wordpress-auth-laravel::auth.id');
		$this->table = Config::get('wordpress-auth-laravel::auth.table');
		$this->hidden = array(Config::get('wordpress-auth-laravel::auth.password'));
	}

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the username for the user.
	 *
	 * @return string
	 */
	public function getUsername()
	{
		$username = Config::get('wordpress-auth-laravel::auth.username');

		return $this->{$username};
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		$password = Config::get('wordpress-auth-laravel::auth.password');

		return $this->{$password};
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		$email = Config::get('wordpress-auth-laravel::auth.email');

		return $this->{$email};
	}

}