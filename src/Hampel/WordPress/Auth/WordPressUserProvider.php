<?php namespace Hampel\WordPress\Auth;
/**
 * 
 */

use Config;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Hashing\HasherInterface;
use Illuminate\Container\Container;

class WordPressUserProvider extends EloquentUserProvider {

	protected $connection;

	protected $container;

	public function __construct(HasherInterface $hasher, $model, $connection)
	{
		$this->connection = $connection;
		parent::__construct($hasher, $model);
	}

	public function retrieveByCredentials(array $credentials)
	{
		// First we will add each credential element to the query as a where clause.
		// Then we can execute the query and, if we found a user, return it in a
		// Eloquent User "model" that will be utilized by the Guard instances.
		$query = $this->createModel()->newQuery();

		$password = $this->container['config']->get('wordpress-auth-laravel::auth.password');

		foreach ($credentials as $key => $value)
		{
			if ( ! str_contains($key, $password)) $query->where($key, $value);
		}

		return $query->first();
	}

	public function validateCredentials(UserInterface $user, array $credentials)
	{
		$plain = $credentials[$this->container['config']->get('wordpress-auth-laravel::auth.password')];

		return $this->hasher->check($plain, $user->getAuthPassword());
	}

	public function createModel()
	{
		$model = parent::createModel();
		$model->setConnection($this->connection);
		return $model;
	}

	/**
	 * Set the IoC container instance.
	 *
	 * @param  \Illuminate\Container\Container  $container
	 * @return void
	 */
	public function setContainer(Container $container)
	{
		$this->container = $container;
	}

}

?>
